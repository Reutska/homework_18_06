package org.homework_18_06.sorting;
import org.homework_18_06.entity.Array;

public class ArraySorting {

    Array array;

    /**
     * Sorts the array into ascending or descending order according to @param sortOrder value.
     *
     * @param array     the array to be sorted
     * @param sortOrder sort direction. Ascending is by default. @param sortOrder can get empty String value.
     */

    public static void bubbleSort(Array array, String sortOrder) {
        int buff = 0;
        for (int i = 0; i < array.getArray().length; i++) {
            for (int j = 0; j < array.getArray().length - i - 1; j++) {
                if (sortOrder.equals("asc") || sortOrder.equals("")) {
                    if (array.getArray()[j] > array.getArray()[j + 1]) {
                        buff = array.getArray()[j];
                        array.getArray()[j] = array.getArray()[j + 1];
                        array.getArray()[j + 1] = buff;
                    }
                } else if (sortOrder.equals("desc")) {
                    if (array.getArray()[j] < array.getArray()[j + 1]) {
                        buff = array.getArray()[j];
                        array.getArray()[j] = array.getArray()[j + 1];
                        array.getArray()[j + 1] = buff;
                    }
                }
            }

        }
    }

    /**
     * Sorts the array into ascending or descending order according to @param sortOrder value.
     *
     * @param array     the array to be sorted
     * @param sortOrder sort direction. Ascending is by default. @param sortOrder can get empty String value.
     */

    public static void selectionSort(Array array, String sortOrder) {
        int buff = 0, minIndex = 0, minElement = 0;
        for (int i = 0; i < array.getArray().length; i++) {
            minIndex = i;
            minElement = array.getArray()[i];
            for (int j = i + 1; j < array.getArray().length; j++) {
                if (sortOrder.equals("asc") || sortOrder.equals("")) {
                    if (array.getArray()[j] < minElement) {
                        minElement = array.getArray()[j];
                        minIndex = j;

                    }
                } else if (sortOrder.equals("desc")) {
                    if (array.getArray()[j] > minElement) {
                        minElement = array.getArray()[j];
                        minIndex = j;
                    }
                }
            }
            if (i != minIndex) {
                buff = array.getArray()[i];
                array.getArray()[i] = array.getArray()[minIndex];
                array.getArray()[minIndex] = buff;

            }


        }
    }

}
