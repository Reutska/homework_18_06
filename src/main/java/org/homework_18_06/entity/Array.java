package org.homework_18_06.entity;

import java.util.Arrays;

public class Array {
     private int[] array;

    public Array(int[] array){
        this.array = array;
    }

    public  int[]  getArray(){
        return array;

    }

    public void setArray(int[] array){
        this.array = array;
    }

    @Override
    public String toString() {
        return "Array{" +
                "array=" + Arrays.toString(array) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Array array1 = (Array) o;
        return Arrays.equals(array, array1.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }
}
