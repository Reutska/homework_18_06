package org.homework_18_06.main;

import org.homework_18_06.entity.Array;
import org.homework_18_06.sorting.ArraySorting;
import org.apache.log4j.Logger;

public class Main {
    private  static  final Logger log = Logger.getLogger(Main.class);
    public static void main (String[] args){
        Array arrayForBubbleSort = new Array(new int[]{7,3,2,8,1,0,6});
        ArraySorting.bubbleSort(arrayForBubbleSort,"asc");
        log.info("Sorting with BubbleSort method");
        System.out.println(arrayForBubbleSort);

        Array arrayForSelectionSort = new Array(new int[]{7,3,2,8,4,0,6});
        ArraySorting.selectionSort(arrayForSelectionSort,"asc");
        log.info("Sorting with SelectionSort method");
        System.out.println(arrayForSelectionSort);

    }
}
