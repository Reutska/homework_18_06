package org.homework_18_06.sorting;

import org.homework_18_06.entity.Array;
import org.junit.Assert;
import org.junit.*;
import org.junit.Test;

public class ArraySortingTest extends Assert {
    private  Array arrayForBubbleSortAsc, arrayForSelectionSortAsc, arrayExpectedBubbleSortAsc, arrayExpectedSelectionSortAsc, arrayForBubbleSortDesc, arrayForSelectionSortDesc, arrayExpectedBubbleSortDesc, arrayExpectedSelectionSortDesc;

    @Before
    public  void setUp() {
        arrayForBubbleSortAsc = new Array(new int[]{7, 3, 2, 8, 4, 0, 6});
        arrayForSelectionSortAsc = new Array(new int[]{7, 3, 2, 8, 4, 0, 6});

        arrayExpectedBubbleSortAsc = new Array(new int[]{0, 2, 3, 4, 6, 7, 8});
        arrayExpectedSelectionSortAsc = new Array(new int[]{0, 2, 3, 4, 6, 7, 8});

        arrayForBubbleSortDesc = new Array(new int[]{7, 3, 2, 8, 4, 0, 6});
        arrayForSelectionSortDesc = new Array(new int[]{7, 3, 2, 8, 4, 0, 6});

        arrayExpectedBubbleSortDesc = new Array(new int[]{8, 7, 6, 4, 3, 2, 0});
        arrayExpectedSelectionSortDesc = new Array(new int[]{8, 7, 6, 4, 3, 2, 0});

    }

    @Test
    public void bubbleSortAscTest() {
        ArraySorting.bubbleSort(arrayForBubbleSortAsc, "asc");
        Assert.assertArrayEquals(arrayExpectedBubbleSortAsc.getArray(), arrayForBubbleSortAsc.getArray());
    }

    @Test
    public void bubbleSortDescTest() {
        ArraySorting.bubbleSort(arrayForBubbleSortDesc, "desc");
        Assert.assertArrayEquals(arrayExpectedBubbleSortDesc.getArray(), arrayForBubbleSortDesc.getArray());
    }

    @Test
    public void selectionSortAscTest() {
        ArraySorting.selectionSort(arrayForSelectionSortAsc, "asc");
        Assert.assertArrayEquals(arrayExpectedSelectionSortAsc.getArray(), arrayForSelectionSortAsc.getArray());
    }

    @Test
    public void selectionSortDescTest() {
        ArraySorting.selectionSort(arrayForSelectionSortDesc, "desc");
        Assert.assertArrayEquals(arrayExpectedSelectionSortDesc.getArray(), arrayForSelectionSortDesc.getArray());
    }

    @After
    public  void tearDown() {
        arrayForBubbleSortAsc = null;
        arrayForSelectionSortAsc = null;

        arrayExpectedBubbleSortAsc = null;
        arrayExpectedSelectionSortAsc = null;

        arrayForBubbleSortDesc = null;
        arrayForSelectionSortDesc = null;

        arrayExpectedBubbleSortDesc = null;
        arrayExpectedSelectionSortDesc = null;

    }
}